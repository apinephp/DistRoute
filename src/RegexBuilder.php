<?php
/**
 * APIne DistRoute
 *
 * @link      https://gitlab.com/apinephp/dist-route
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/dist-route/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\DistRoute;

/**
 * Class RegexBuilder
 *
 * @package Apine\DistRoute
 */
class RegexBuilder implements RegexBuilderInterface
{
    /**
     * @var ArgumentParserInterface
     */
    private $parser;
    
    /**
     * @var string
     */
    private $masterPattern;
    
    public function __construct(ArgumentParserInterface $parser, string $pattern)
    {
        $this->parser = $parser;
        $this->masterPattern = $pattern;
    }
    
    public function build(): string
    {
        $pattern = $this->masterPattern;
        $arguments = $this->parser->parse();
        
        $regex = '/^' . str_ireplace('/', '\\/', $pattern) . '$/';
    
        array_walk($arguments, function (Argument $argument) use (&$regex): void {
            if ($argument->optional) {
                $match = '/\\\\\\/\{\?' . $argument->name . '(:(\(.+?\)))?\}/'; // Five backslashes to match a single backslash, really????? At least it works that way... right?
                $regex = preg_replace($match, '(\/' . $argument->pattern . ')?', $regex);
            } else {
                $regex = preg_replace('/\{' . $argument->name . '(:(\(.+?\)))?\}/', $argument->pattern, $regex);
            }
        });
    
        return $regex;
    }
}