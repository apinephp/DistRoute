<?php
/**
 * APIne DistRoute
 *
 * @link      https://gitlab.com/apinephp/dist-route
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/dist-route/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\DistRoute;

use Psr\Http\Message\ServerRequestInterface;

/**
 * Interface ArgumentExtractorInterface
 *
 * @package Apine\DistRoute
 */
interface ArgumentExtractorInterface
{
    /**
     * Extract route's arguments from the request
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     *
     * @return array<string,string> An array of the values extracted from the request
     */
    public function extract(ServerRequestInterface $request): array;
}