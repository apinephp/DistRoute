<?php
/**
 * APIne DistRoute
 *
 * @link      https://gitlab.com/apinephp/dist-route
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/dist-route/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\DistRoute;

use Closure;
use Exception;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use RuntimeException;
use Apine\Resolver\DependencyResolver;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use function sprintf;
use function is_callable, is_string;

/**
 * Route
 *
 * @package Apine\DistRoute
 */
final class Route implements RouteInterface
{
    /**
     * Route's pattern
     *
     * @var string
     */
    public $pattern;
    
    /**
     * Accepted request methods
     *
     * @var string[]
     */
    public $methods;
    
    /**
     * Handler to be executed
     *
     * @var callable|string
     */
    private $callable;
    
    /**
     * @var DependencyResolver
     */
    private $dependencyResolver;
    
    /**
     * The array of accepted methods MUST include any standard or custom capitalized
     * method names as defined in RFC-7231. Leaving the list empty means that
     * the route accepts any request method.
     *
     * The route pattern is a string similar to the uri path as defined in
     * RFC-3986. The parts that MAY be matched and used as parameter for the
     * callable MUST be declared with a name in between curly brackets. A
     * matching pattern as a valid regular expression part MAY by included in
     * parentheses and separated from the parameter name by a colon. A parameter
     * MAY be explicitly identified as optional by placing a question mark before
     * the name of the parameter.
     *
     * Examples of valid route patterns :
     *
     * - /users
     * - /user/{id}
     * - /user/{id:(\d+)}
     * - /user/{id}/{?other}
     *
     * The callable is either an anonymous function, the name of a function, or
     * the name of a class and the name of the method separated with a "@". The
     * name of the controller MUST include be fully qualified (the namespace as
     * well as the class name).
     *
     * @see https://tools.ietf.org/html/rfc7231#section-4
     * @see https://tools.ietf.org/html/rfc3986#section-3.3
     *
     * @param DependencyResolver                  $dependencyResolver
     * @param string[]                            $methods A list of accepted request methods
     * @param string                              $pattern A route pattern similar to /user/{id:(\d+)}
     * @param callable|string|MiddlewareInterface $callable The handler to be executed
     */
    public function __construct(DependencyResolver $dependencyResolver, array $methods, string $pattern, $callable)
    {
        $this->dependencyResolver = $dependencyResolver;
        $this->pattern = $pattern;
        $this->methods = $methods;
        $this->callable = $callable;
    }
    
    /**
     * Verify if the request matches the route
     *
     * @param ServerRequestInterface $request
     *
     * @return boolean TRUE if the the request string and the method match the route
     */
    public function match(ServerRequestInterface $request): bool
    {
        $validator = new RequestValidator(
            new RegexBuilder(
                new ArgumentParser($this->pattern),
                $this->pattern
            ),
            $this->methods
        );
    
        return $validator->match($request);
    }
    
    /**
     * Execute the handler
     *
     * @param ServerRequestInterface  $request
     *
     * @return ResponseInterface
     * @throws Exception
     */
    public function invoke(ServerRequestInterface $request): ResponseInterface
    {
        $resolver = $this->dependencyResolver;
        
        $extractor = new ArgumentExtractor(
            new ArgumentParser($this->pattern),
            new RegexBuilder(
                new ArgumentParser($this->pattern),
                $this->pattern
            )
        );
        $queryArguments = $extractor->extract($request);
        
        if (is_callable($this->callable)) {
            $response = $resolver->call($this->callable, $queryArguments);
        } else if (is_string($this->callable)) {
            if (strpos($this->callable, '@')) {
                [$controllerName, $method] = explode('@', $this->callable);
                
                if (class_exists($controllerName) && method_exists($controllerName, $method)) {
                    $response = $resolver->execute(
                        $method,
                        $resolver->createInstance($controllerName),
                        $queryArguments
                    );
                } else {
                    throw new RuntimeException(sprintf('Method %s::%s not found', $controllerName, $method));
                }
            } else {
                $response = $resolver->call($this->callable, $queryArguments);
            }
        } else {
            throw new RuntimeException('The callable must be an instance of Closure or a reference to a controller class');
        }
        
        if (!($response instanceof ResponseInterface)) {
            if ($this->callable instanceof Closure) {
                $name = 'Closure';
            } else {
                $name = $this->callable;
            }
            
            throw new RuntimeException(sprintf('%s must return an instance of %s', $name, ResponseInterface::class));
        }
        
        return $response;
    }
    
    /**
     * Process an incoming server request and return a response, optionally delegating
     * response creation to a handler.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Server\RequestHandlerInterface $handler
     *
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if ($this->match($request)) {
            if ($this->callable instanceof MiddlewareInterface) {
                return $this->callable->process($request, $handler);
            }
            
            return $this->invoke($request);
        }
    
        return $handler->handle($request);
    }
}