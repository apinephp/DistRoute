<?php
/**
 * APIne DistRoute
 *
 * @link      https://gitlab.com/apinephp/dist-route
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/dist-route/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\DistRoute;

use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Interface RouteInterface
 *
 * @package Apine\DistRoute
 */
interface RouteInterface extends MiddlewareInterface
{
    public function match(ServerRequestInterface $serverRequest): bool;
    
    public function invoke(ServerRequestInterface $serverRequest): ResponseInterface;
}