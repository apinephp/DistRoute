<?php
/**
 * APIne DistRoute
 *
 * @link      https://gitlab.com/apinephp/dist-route
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/dist-route/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\DistRoute;

/**
 * Class Argument
 *
 * @package Apine\DistRoute
 */
class Argument
{
    /**
     * @var string
     */
    public $name;
    
    /**
     * @var string
     */
    public $pattern;
    
    /**
     * @var bool
     */
    public $optional;
    
    public function __construct(
        string $name,
        string $pattern,
        bool $optional
    )
    {
        $this->name = $name;
        $this->pattern = $pattern;
        $this->optional = $optional;
    }
}