<?php
/**
 * APIne DistRoute
 *
 * @link      https://gitlab.com/apinephp/dist-route
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/dist-route/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

use Apine\DistRoute\Argument;
use Apine\DistRoute\ArgumentExtractor;
use Apine\DistRoute\ArgumentParserInterface;
use Apine\DistRoute\RegexBuilderInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriInterface;

class ArgumentExtractorTest extends TestCase
{
    public function testExtract(): void
    {
        $extractor = new ArgumentExtractor(
            $this->mockParser(),
            $this->mockBuilder()
        );
        
        $result = $extractor->extract($this->mockRequest());
        
        // Validate format is array[string][string}
        $this->assertNotEmpty($result);
        $this->assertArrayHasKey('input', $result);
        $this->assertThat($result['input'], $this->isType('string'));
    }
    
    public function testExtractWhenRequestDoesNotMatchShouldReturnEmptyArray(): void
    {
        $extractor = new ArgumentExtractor(
            $this->mockParser(),
            $this->mockBuilder()
        );
    
        /** @var ServerRequestInterface | MockObject $request */
        $request = $this->getMockBuilder(ServerRequestInterface::class)
            ->setMethods(['getUri'])
            ->getMockForAbstractClass();
        $request->method('getUri')->willReturnCallback(function() {
            /** @var UriInterface | MockObject $mockUri */
            $mockUri = $this->getMockBuilder(UriInterface::class)
                ->setMethods(['getPath'])
                ->getMockForAbstractClass();
            $mockUri->method('getPath')->willReturn('/123456789');
            return $mockUri;
        });
    
        $result = $extractor->extract($request);
    
        $this->assertEmpty($result);
    }
    
    private function mockParser(): ArgumentParserInterface
    {
        /** @var ArgumentParserInterface | MockObject $mockParser */
        $mockParser = $this->getMockBuilder(ArgumentParserInterface::class)
            ->setMethods(['parse'])
            ->getMockForAbstractClass();
        $mockParser->method('parse')->willReturn([
            $this->getMockBuilder(Argument::class)->setConstructorArgs([
                'input',
                '([0-9]+)',
                false
            ])->getMock(),
            $this->getMockBuilder(Argument::class)->setConstructorArgs([
                'option',
                '([^\/]+?)',
                true
            ])->getMock()
        ]);
        
        return $mockParser;
    }
    
    private function mockBuilder(): RegexBuilderInterface
    {
        /** @var RegexBuilderInterface | MockObject $mockBuilder */
        $mockBuilder = $this->getMockBuilder(RegexBuilderInterface::class)
            ->setMethods(['build'])
            ->getMockForAbstractClass();
        $mockBuilder->method('build')->willReturn('/^\/test\/([0-9]+)(\/([^\/]+?))?$/');
        
        return $mockBuilder;
    }
    
    private function mockRequest(): ServerRequestInterface
    {
        /** @var ServerRequestInterface | MockObject $mockRequest */
        $mockRequest = $this->getMockBuilder(ServerRequestInterface::class)
            ->setMethods(['getUri', 'getMethod'])
            ->getMockForAbstractClass();
        $mockRequest->method('getMethod')->willReturn('GET');
        $mockRequest->method('getUri')->willReturnCallback(function() {
            /** @var UriInterface | MockObject $mockUri */
            $mockUri = $this->getMockBuilder(UriInterface::class)
                ->setMethods(['getPath'])
                ->getMockForAbstractClass();
            $mockUri->method('getPath')->willReturn('/test/123456789');
            return $mockUri;
        });
        
        return $mockRequest;
    }
}
